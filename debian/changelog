geoipupdate (7.1.0-1) unstable; urgency=medium

  * New upstream release.

 -- Faidon Liambotis <paravoid@debian.org>  Tue, 19 Nov 2024 19:11:41 +0200

geoipupdate (7.0.1-1) unstable; urgency=medium

  * New upstream release.
    - Add new build dependency golang-golang-x-net-dev.
  * Bump Standards-Version to 4.7.0, no changes needed.

 -- Faidon Liambotis <paravoid@debian.org>  Mon, 15 Apr 2024 17:16:18 +0300

geoipupdate (6.1.0-1) unstable; urgency=medium

  * New upstream release.
  * Add RandomizedDelaySec=3h to the default timer unit, per upstream's
    request (GitHub issue #122).
  * Rename "MIT" to "Expat" in debian/copyright to match Debian's label for
    what is commonly known as the MIT license.

 -- Faidon Liambotis <paravoid@debian.org>  Fri, 12 Jan 2024 06:57:53 +0200

geoipupdate (6.0.0-2) unstable; urgency=medium

  * Add a dependency on ca-certificates, to avoid "unknown authority"
    errors when validating updates.maxmind.com. (Closes: #1052387)
  * Change the timer unit to refresh twice a week (Wed/Sat 00:00 Eastern), per
    MaxMind's new published schedule. See upstream issue #122 for context.

 -- Faidon Liambotis <paravoid@debian.org>  Sat, 28 Oct 2023 18:36:32 +0300

geoipupdate (6.0.0-1) unstable; urgency=medium

  * New upstream release.
    - Drop obsolete patch "v4.10-version"
  * Add new build dependencies golang-github-cenkalti-backoff-dev and
    golang-golang-x-sync-dev.
  * Switch to DEP-14 branch naming.
  * Bump Standards-Version to 4.6.2, no changes needed.

 -- Faidon Liambotis <paravoid@debian.org>  Fri, 14 Jul 2023 03:24:08 +0300

geoipupdate (4.10.0-1) unstable; urgency=medium

  * New upstream release.
    - Backport a patch from upstream to bump version.go to 4.10, as the
      tarball was created two commits too early.
  * Update upstream's homepage in debian/control as well as GeoIP.conf.
  * Add systemd hardening directives to the service file. Thanks to Christian
    Göttsche for the contribution. (Closes: #979722)
  * Switch from Built-Using to the new Static-Built-Using field, per golang's
    latest packaging standard.
  * Switch Build-Depends from dh-golang to dh-sequence-golang.
  * Add support for the nodoc profile, disabling the manpage generation
    through pandoc.
  * Bump Standards-Version to 4.6.1, no further changes needed.

 -- Faidon Liambotis <paravoid@debian.org>  Sun, 08 Jan 2023 08:36:34 +0200

geoipupdate (4.6.0-1) unstable; urgency=medium

  [ Faidon Liambotis ]
  * New upstream version.
  * Reflect changes with regards to GeoLite2's availability. (Closes: #948469)
    - Adjust the package description to reflect that an account is now needed.
    - Adjust the default GeoIP.conf to remove the now defunct default account.
    - Add a README.Debian to help guide new users on how to use this package.
    - Add a condition to the shipped cron file to avoid running geoipupdate
      when no accounts have been configured.
  * Stop referring to deprecated GeoIP Legacy products, or Suggest geoip-bin.
  * Add a systemd service and timer as an alternative to cron for systemd
    systems.
  * Bump debhelper-compat to 13.
  * Update standards version to 4.5.1, no changes needed.

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update standards version to 4.5.0, no changes needed.

 -- Faidon Liambotis <paravoid@debian.org>  Sat, 09 Jan 2021 18:02:37 +0200

geoipupdate (4.0.6-2) unstable; urgency=high

  * Do not attempt to build PIE binaries on mips architectures, as this is not
    supported by Go yet.
  * Ship with an empty /var/lib/GeoIP for geoipupdate to place the databases.
    (Closes: #943849)
  * Bump Standards-Version to 4.4.1, no changes needed.

 -- Faidon Liambotis <paravoid@debian.org>  Wed, 30 Oct 2019 21:46:20 +0200

geoipupdate (4.0.6-1) unstable; urgency=medium

  * New upstream release.
    - Entirely rewritten from scratch in a different language (Go).
    - Now dual-licensed under Apache 2.0/MIT.
  * Update watch file to v4 and to cover the new upstream filename pattern.
  * Largely rewrite the Debian packaging for the new upstream in Go.

 -- Faidon Liambotis <paravoid@debian.org>  Sat, 14 Sep 2019 20:30:31 +0300

geoipupdate (3.1.1-2) unstable; urgency=medium

  * Final release of the geoiupdate 3.x series.
  * Explicitly mention GeoLite2 in the package description.
  * Remove GeoLite Legacy example edition IDs from the config, as those were
    discontinued on January 2nd, 2019.
  * Use https (and not http) for upstream's Homepage.
  * Bump debhelper compat level to 12.
  * Bump Standards-Version to 4.4.0, no changes needed.

 -- Faidon Liambotis <paravoid@debian.org>  Sat, 14 Sep 2019 20:22:05 +0300

geoipupdate (3.1.1-1) unstable; urgency=medium

  * New upstream release.
    - Upstream's build system now ships /var/lib/GeoIP properly, so remove our
      own customizations to achieve the same.
    - Update the default GeoIP.conf with upstream's new recommendations e.g.
      around AccountID and LicenseKey.
  * Point Vcs-* to the package's new home on salsa.debian.org.
  * Multiple updates on debian/copyright:
    - Add Comment to explain why this package is in "contrib".
    - Remove geoipupdate-pureperl.pl not shipped by upstream anymore.
    - Use https to link to debian.org copyright-format documentation.
    - Update copyright years.
  * Bump debhelper compat to 11.
  * Bump Standards-Version to 4.3.0, no changes needed.

 -- Faidon Liambotis <paravoid@debian.org>  Mon, 07 Jan 2019 23:49:49 +0200

geoipupdate (2.5.0-1) unstable; urgency=medium

  * New upstream release.
  * Adjust the long description to mention that this package can be used for
    the free GeoLite editions.
  * Ship a default /etc/GeoIP.conf, with GeoLite2 databases enabled and
    references to GeoLite Legacy databases.
  * Ship a default /etc/cron.d/geoipupdate that runs geoiupdate on a weekly
    basis.
  * Stop shipping an empty /usr/share/GeoIP, as that path is not used by this
    package. This was introduced erroneously by upstream in 2.4.0.
  * Remove Breaks/Conflicts with a pre-jessie version of geoip-bin (1.6.1-1).
  * Bump Standards-Version to 4.1.2, no changes needed.
  * Remove bin/base64.{c,h} from debian/copyright, not included with upstream
    anymore.

 -- Faidon Liambotis <paravoid@debian.org>  Wed, 27 Dec 2017 05:59:43 +0200

geoipupdate (2.4.0-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version to 4.0.0.
  * Remove --with autoreconf from dh's invocation, as this is now implied.

 -- Faidon Liambotis <paravoid@debian.org>  Fri, 04 Aug 2017 23:04:05 +0300

geoipupdate (2.3.1-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version to 3.9.8, no changes needed.
  * Bump debhelper compat level to 10; drop dh-autoreconf Build-Dep.
  * Cleanup upstream-installed files from /usr/share/doc/.

 -- Faidon Liambotis <paravoid@debian.org>  Wed, 25 Jan 2017 10:56:07 +0200

geoipupdate (2.2.2-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version to 3.9.7, no changes needed.
  * Switch Homepage, Vcs-Git and Vcs-Browser to https URLs.

 -- Faidon Liambotis <paravoid@debian.org>  Wed, 23 Mar 2016 03:05:43 +0200

geoipupdate (2.2.1-1) unstable; urgency=medium

  * Initial release. (Closes: #768979)

 -- Faidon Liambotis <paravoid@debian.org>  Sun, 19 Jul 2015 21:39:18 +0300
